package com.matm.matmsdk.dmtModule.bankList;

import java.io.Serializable;

public class DMTBankModel implements Serializable {
    private String bankName;
    private String IMPS_AVAILABILITY;
    private String BANKCODE;
    private String ACCOUNT_VERAFICATION_AVAILABLE;
    private String FLAG;
    private String PATTERN;
    private Boolean active;
    private String NEFT_AVAILABILITY;


    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getIMPS_AVAILABILITY() {
        return IMPS_AVAILABILITY;
    }

    public void setIMPS_AVAILABILITY(String IMPS_AVAILABILITY) {
        this.IMPS_AVAILABILITY = IMPS_AVAILABILITY;
    }

    public String getBANKCODE() {
        return BANKCODE;
    }

    public void setBANKCODE(String BANKCODE) {
        this.BANKCODE = BANKCODE;
    }

    public String getACCOUNT_VERAFICATION_AVAILABLE() {
        return ACCOUNT_VERAFICATION_AVAILABLE;
    }

    public void setACCOUNT_VERAFICATION_AVAILABLE(String ACCOUNT_VERAFICATION_AVAILABLE) {
        this.ACCOUNT_VERAFICATION_AVAILABLE = ACCOUNT_VERAFICATION_AVAILABLE;
    }

    public String getFLAG() {
        return FLAG;
    }

    public void setFLAG(String FLAG) {
        this.FLAG = FLAG;
    }

    public String getPATTERN() {
        return PATTERN;
    }

    public void setPATTERN(String PATTERN) {
        this.PATTERN = PATTERN;
    }

    public String getNEFT_AVAILABILITY() {
        return NEFT_AVAILABILITY;
    }

    public void setNEFT_AVAILABILITY(String NEFT_AVAILABILITY) {
        this.NEFT_AVAILABILITY = NEFT_AVAILABILITY;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
