package com.matm.matmsdk.dmtModule;

import android.bluetooth.BluetoothDevice;

import com.matm.matmsdk.dmtModule.bankList.DMTBankModel;

import java.util.ArrayList;
import java.util.List;

public class DmtConstants {
    public static final String WALLET2_TRANSACTION_API = "https://dmt.iserveu.tech/generate/v42";
    public static final String WALLET3_TRANSACTION_API = "https://dmt.iserveu.tech/generate/v83";
    public static final String BULK_TRANSFER = "https://dmt.iserveu.tech/generate/v5";
    public static final String BULK_TRANSFER_WALLET3 = "https://dmt.iserveu.tech/generate/v84";
    public static final String VERIFY_BENE_URL = "https://dmt.iserveu.tech/generate/v17";
    public static final String ADD_AND_PAY = "https://dmt.iserveu.tech/generate/v6";
    public static final String ADD_BENE_URL = "https://dmt.iserveu.tech/generate/v13";

    public static final String ADD_CUSTOMER = "https://dmt.iserveu.tech/generate/v14";
    public static final String DELETE_BENE_DMT = "https://dmt.iserveu.tech/generate/v25";
    public static final String RESEND_OTP = "https://dmt.iserveu.tech/generate/v19";
    public static final String TRANSACTION = "https://dmt.iserveu.tech/generate/v4";
    public static final String VERIFY_OTP = "https://dmt.iserveu.tech/generate/v18";
    public static final String GET_LOAD_WALLET_URL = "https://dmt.iserveu.tech/generate/v20";

    public static final String BASE_URL = "https://mobile.9fin.co.in/";


    public static BluetoothDevice bluetoothDevice= null;
    public static String SHOP_NAME="";
    public static String BRAND_NAME="";
    public static DMTBankModel BANK_FLAG = new DMTBankModel();
    public static String selectBank = "";
    public static List<DMTBankModel> flag_list = new ArrayList<>();
    public static ArrayList<String> user_feature_array= new ArrayList<>();

    public static String tokenFromCoreApp ="";
    public static String userNameFromCoreApp ="";
    public static String applicationType ="";

    public static String token = "";
    public static String loginID = "";
    public static String encryptedData = "";


}
