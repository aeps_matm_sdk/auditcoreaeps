package com.matm.matmsdk.dmtModule.bankList;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.matm.matmsdk.Utils.SdkConstants;

import java.util.ArrayList;
import java.util.List;

import isumatm.androidsdk.equitas.R;

public class DMTBankNameActivity extends AppCompatActivity implements DMTBankContract.View {
    private List<DMTBankModel> bankNameModelList = new ArrayList<>();
    private RecyclerView bankNameRecyclerView;
    private DMTBankAdapter bankNameListAdapter;
    private DMTBankPresenter bankNameListPresenter;
    ShimmerFrameLayout bankNameShimmer;
    EditText searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dmt_activity_bank_name_list);
        bankNameShimmer = findViewById(R.id.bankNameShimmerLayoout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        changeStatusBar(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bankNameShimmer.isShimmerStarted()) {
                    onBackPressed();
                }
            }
        });

        bankNameRecyclerView = (RecyclerView) findViewById(R.id.bankNameRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        bankNameRecyclerView.setLayoutManager(mLayoutManager);
        bankNameRecyclerView.setItemAnimator(new DefaultItemAnimator());

        bankNameListPresenter = new DMTBankPresenter(this);
        bankNameListPresenter.loadBankNamesList(this);


        searchView = findViewById(R.id.searchView);
        searchView.setFocusable(true);
        searchView.clearFocus();
        searchView.requestFocusFromTouch();



        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s!=null){
                    if(!TextUtils.isEmpty(s) && bankNameListAdapter!= null) {
                        bankNameListAdapter.getFilter().filter(s);
                    }
                }
            }
        });

    }


    @Override
    public void bankNameListReady(ArrayList<DMTBankModel> bankNameModelArrayList) {
        if (bankNameModelArrayList!=null && bankNameModelArrayList.size() > 0){
            bankNameModelList = bankNameModelArrayList;
        }
    }

    @Override
    public void showBankNames() {
        if (bankNameModelList!=null && bankNameModelList.size() > 0){
            bankNameListAdapter = new DMTBankAdapter(bankNameModelList, new DMTBankAdapter.RecyclerViewClickListener() {
                @Override
                public void recyclerViewListClicked(View v, int position) {
                    SdkConstants.BANK_FLAG = bankNameListAdapter.getItem(position);
                    SdkConstants.selectBank = bankNameListAdapter.getItem(position).getBankName();
                    SdkConstants.flag_list = bankNameModelList;
                    finish();
//                    intent.putExtra(AepsSdkConstants.IIN_KEY, bankNameListAdapter.getItem(position));
//                    setResult(RESULT_OK, intent);
//                    finish();
                }
            });
            bankNameRecyclerView.setAdapter ( bankNameListAdapter );
        }

    }

    @Override
    public void showLoader() {
        if (!bankNameShimmer.isShimmerStarted()) {
            bankNameShimmer.startShimmer();
            bankNameShimmer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoader() {
        if (bankNameShimmer.isShimmerStarted()) {
            bankNameShimmer.stopShimmer();
        }
        bankNameShimmer.setVisibility(View.GONE);
    }

    @Override
    public void emptyBanks() {

    }

    @Nullable
    @Override
    public ActionBar getSupportActionBar() {
        return super.getSupportActionBar();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode== KeyEvent.KEYCODE_BACK)
            return false;

        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    public void changeStatusBar(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getWindow().setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        }
    }
}