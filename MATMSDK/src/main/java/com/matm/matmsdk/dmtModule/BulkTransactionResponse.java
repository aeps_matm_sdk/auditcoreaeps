package com.matm.matmsdk.dmtModule;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class BulkTransactionResponse implements Serializable {

    @SerializedName("statusWithBeneInfo")
    ArrayList<TransactionResponse> transactionResponse ;

    public ArrayList<TransactionResponse> getTransactionResponse() {
        return transactionResponse;
    }

    public void setTransactionResponse(ArrayList<TransactionResponse> transactionResponse) {
        this.transactionResponse = transactionResponse;
    }
}
