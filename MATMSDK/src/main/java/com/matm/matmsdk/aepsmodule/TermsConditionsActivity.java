package com.matm.matmsdk.aepsmodule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.matm.matmsdk.Utils.SharedPreferenceClass;

import isumatm.androidsdk.equitas.R;

public class TermsConditionsActivity extends AppCompatActivity {

    CheckBox firststText,secondstText;
    SharedPreferenceClass sharedPreferenceClass;
    SwitchCompat switchCompat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);

        firststText = findViewById(R.id.firststText);
        secondstText = findViewById(R.id.secondstText);

        switchCompat = findViewById(R.id.swOnOff);

        sharedPreferenceClass = new SharedPreferenceClass(TermsConditionsActivity.this);

        firststText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                firststText.setChecked(true);
                sharedPreferenceClass.setBoolen(true);
            }
        });
        secondstText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                secondstText.setChecked(true);
                sharedPreferenceClass.setBoolen(true);
            }
        });





        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    firststText.setText(getResources().getString(R.string.hinditm1));
                    secondstText.setText(getResources().getString(R.string.hinditm2));

                }else{
                    firststText.setText(getResources().getString(R.string.term1));
                    secondstText.setText(getResources().getString(R.string.term2));

                }
            }
        });

    }
}