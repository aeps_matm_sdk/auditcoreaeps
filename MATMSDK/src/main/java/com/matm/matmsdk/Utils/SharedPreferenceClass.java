package com.matm.matmsdk.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;


public class SharedPreferenceClass {
    private static final String USER_PREFS = "ISERVEU";
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;

    public SharedPreferenceClass(Context context) {
        this.appSharedPrefs = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }

    public boolean getValue_bool(Boolean value){
        return appSharedPrefs.getBoolean(String.valueOf(value),false );
    }

    public void setBoolen(Boolean BoolVal){
        prefsEditor.putBoolean(String.valueOf(BoolVal), false).commit();
    }

    public int getValue_int(String intKeyValue) {

        return appSharedPrefs.getInt(intKeyValue, 0);
    }

    public String getValue_string(String stringKeyValue) {
        return appSharedPrefs.getString(stringKeyValue, "");
    }

    public void setValue_int(String intKeyValue, int _intValue) {

        prefsEditor.putInt(intKeyValue, _intValue).commit();
    }

    public void setValue_string(String stringKeyValue, String _stringValue) {

        prefsEditor.putString(stringKeyValue, _stringValue).commit();

    }

    public void setValue_int(String intKeyValue) {

        prefsEditor.putInt(intKeyValue, 0).commit();
    }

    public void clearData() {
        prefsEditor.clear().commit();

    }


}

